/**
* Main App
*/
import React from 'react';
import { Provider } from 'react-redux';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';


// css
import './assets/custom.css';

// app component
import Dashboard from './View/Dashboard';

//import { configureStore } from './store';
import store from './store';

const MainApp = () => {
	

	return (<Provider store={store}>
			<Router>
				<Switch>
					<Route path="/" component={Dashboard} />
				</Switch>
			</Router>
	</Provider>)
};

export default MainApp;