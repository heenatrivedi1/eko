import React from 'react'
import { connect } from 'react-redux';
import { increment, decrement, incrementIfOdd, incrementAsync } from "../../redux/action/actions"
const Dashboard = (props) => {
    const { Reducer } = props
    const onIncrement = () => {
        props.increment()
    }
    const onDecrement = () => {
        props.decrement()
    }
    const onIncrementIfOdd = () => {
        props.incrementIfOdd()
    }
    const onIncrementAsync = () => {
        props.incrementAsync()
    }
    return (
        <div>
            <p>
                Clicked: {Reducer.count} times <button onClick={onIncrement}>+</button> <button onClick={onDecrement}>-</button>{' '}
                <button onClick={onIncrementIfOdd}>Increment if odd</button>{' '}
                <button onClick={onIncrementAsync}>Increment async</button>
            </p>
        </div>
    )
}
const mapStateToProps = (state) => {
    return {
        Reducer: state.Reducer,

    };
};

const mapDispatchtoProps = {
    increment,
    decrement,
    incrementIfOdd,
    incrementAsync

};


export default connect(
    mapStateToProps,
    mapDispatchtoProps
)(Dashboard);
