import { all, call, put, takeEvery, delay } from 'redux-saga/effects';
import * as types from "../redux/action/types"

 function* incrementAsync() {
  //yield call(delay, 1000)
  yield delay(1000)
  yield put({ type: types.INCREMENT })
}

function* watchIncrementAsync() {
  yield takeEvery(types.INCREMENT_ASYNC, incrementAsync)
}
export default function* rootSaga() {
    yield all([watchIncrementAsync()]);
  }