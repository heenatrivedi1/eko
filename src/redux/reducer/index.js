/**
 * App Reducers
 */
import { combineReducers } from "redux";
import Reducer from "./Reducer"
const reducers = combineReducers({
    Reducer: Reducer
  });
  
  export default reducers;