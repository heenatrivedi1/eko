import * as types from "../action/types";

const intialState = {
    count:0
 }
const Reducer =(state = intialState, action) => {
  switch (action.type) {
    case types.INCREMENT:
      return { ...state,
          count: state.count + 1};
    case types.INCREMENT_IF_ODD:
      return {...state, count:( state.count % 2 !== 0) ?  state.count + 1 :  state.count};
    case types.DECREMENT:
      return {...state,
          count: state.count - 1}
    default:
      return state
  }
}
export default Reducer;
