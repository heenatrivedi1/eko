import * as types from "./types"

export function increment() {
    return { type: types.INCREMENT };
  }
  
  export function decrement() {
    return { type: types.DECREMENT };
  }
  
  export function incrementIfOdd() {
    return { type: types.INCREMENT_IF_ODD };
  }
  
  export function incrementAsync() {
    return { type: types.INCREMENT_ASYNC };
  }
  
